////////////////////////////////////////////////////////////////////////////
////   Library for ADXL345                                              ////
////                                                                    ////
////   init_ext_eeprom();    Call before the other functions are used   ////
////                  must be called after a 1.1ms delay. All           ////
////                  parameters of the ADXL can be set here            ////
////                                                                    ////
////   write_adxl(a, d);  Write the byte d to the address a             ////
////                                                                    ////
////   d = read_adxl(a);  Read the byte d from the address a            ////
////                                                                    ////
////   read_XYZ(d);  reads the 6 x,y & z bytes from the ADXL and        ////
////                 stores in address passed to function.              ////
////                                                                    ////
////   Tom Griffiths - Coredon Design Ltd 2011.                         ////
////////////////////////////////////////////////////////////////////////////

#include "adxl345.h"

#ifndef EEPROM_SDA

#define I2C_SDA  PIN_B1             //********* DEFINE I2C PINS HERE *********
#define I2C_SCL  PIN_B4             //

#endif

#use i2c(master, sda=I2C_SDA, scl=I2C_SCL)

int device = 0x53;                  //alt address pin to GND, 0X1D if high

void write_adxl(byte address, BYTE data) {
   i2c_start();
   i2c_write(device<<1|0);            //address is 7 bytes, rotate left with 0 as LSB
   i2c_write(address);               //for write commands
   i2c_write(data);
   i2c_stop();
}

byte read_adxl(byte address) {
byte data;
   i2c_start();
   i2c_write(device<<1|0);
   i2c_write(address);
   i2c_start();
   i2c_write(device<<1|1);            //LSB is 1 for read commands
   data=i2c_read(0);
   i2c_stop();
   return(data);
}


void read_XYZ(int *data){            //routine will read 6 bytes from ADXL
int i = 0;                        //X0 to Z1. Returns data to 6 bytes via pointer.
   i2c_start();
   i2c_write(device<<1|0);
   i2c_write(DATAX0);               //1st reg. to be read is DATAX0 (0X32)

   i2c_start();
   i2c_write(device<<1|1);

for (i=0; i<5; i++)                  //read 5 bytes with ACK
   data[i]=i2c_read(1);
   data[i]=i2c_read(0);               //and 6th with NACK
   i2c_stop();
}

byte init_adxl() {                  //must be a delay of 1.1ms before init. routine
byte error;
   error = read_adxl(DEVID)-0xE5;               //DEVID is set as 0x35 for all devices. function should return
                                                //0x00 if device is connected and ok.
   write_adxl(POWER_CTL, (MEASURE|WAKEUP_1HZ));   //These 3 lines are the miniumum initialization
   write_adxl(INT_ENABLE,(DATAREADY));            //required to start the device. Read DATAREADY to sync PIC with output
   write_adxl(DATA_FORMAT,(RANGE_16G|DATA_JUST_RIGHT|FULL_RESOLUTION));
   write_adxl(INT_MAP,(0x00));         //map interrupts to either INT1 or INT2
   write_adxl(DUR, 0x00);            //625us per LSB
   write_adxl(LATENT, 0x00);         //1.25ms per LSB
   write_adxl(WINDOW, 0x00);         //1.25ms per LSB
   write_adxl(THRESH_TAP, 0x00);      //62.5mg per LSB
   write_adxl(TAP_AXES,0x00);         //X,Y or Z
   write_adxl(THRESH_ACT, 0x00);      //62.5mg per LSB
   write_adxl(THRESH_INACT, 0x00);      //62.5mg per LSB
   write_adxl(TIME_INACT, 0x00);      //1s per LSB
   write_adxl(ACT_INACT_CTL, 0x00);   //X,Y or Z
   write_adxl(THRESH_FF, 0X00);      //Values between 300 mg and 600 mg (0x05 to 0x09) are recommended.
   write_adxl(TIME_FF, 0X00);         //Values between 100 ms and 350 ms (0x14 to 0x46) are recommended.
   write_adxl(BW_RATE, (RATE_100));   //Set sampling rate. 800Hz is limit for normal I2C
   return(error);
}
