#include <16f1827.h>
//The following allocates 2 pins for the I2C bus - setup your pins in the file
#include "ADXL345.c"

#fuses MCLR,INTRC_IO,NOWDT,NOPROTECT,NOBROWNOUT,NOPUT
#use delay(clock=4MHz)

#use rs232(baud=9600, xmit=PIN_B2, STREAM=PC)

void main(){
  char xyz_data[6];
  int   data = 1;
  signed long   x,y,z,total,old;
  delay_us(1100);
  fprintf(PC, "\r\nInitialising");
  while (data!=0)data = init_adxl();     //wait until ADXL345 returns correct ID.

  while(1){
    read_XYZ(xyz_data);               //data is returned as 3 x 12bit 2's complement values
    x = make16(xyz_data[1],xyz_data[0]);   //concatanate two values to make signed int16
    y = make16(xyz_data[3],xyz_data[2]);
    z = make16(xyz_data[5],xyz_data[4]);
    //fprintf(PC, "\nX: %X , Y: %X , Z: %X", x,y,z);   //Print data in HEX

    total = x+y+z;

    if (abs(total - old) > 10) {
      fprintf(PC, "\r\nTotal: %ld", total);
      old = total;
    }
  }
}



