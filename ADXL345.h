////////////////////////////////////////////////////////////////////////////
////   Library for ADXL345			                                 	////
////                                                                   	////
////	Tom Griffiths - Coredon Design Ltd 2011.						////
////////////////////////////////////////////////////////////////////////////


#define	DEVID		0x00
#define	THRESH_TAP	0x1D
#define	OFSX		0x1E
#define	OFSY		0x1F
#define	OFSZ		0x20
#define	DUR			0x21
#define Latent		0x22
#define	Window		0x23

#define	THRESH_ACT	0x24
#define	THRESH_INACT	0x25
#define	TIME_INACT	0x26
#define	ACT_INACT_CTL	0x27
#define	THRESH_FF	0x28
#define	TIME_FF		0x29

#define	TAP_AXES	0x2A
#define	ACT_TAP_STATUS	0x2B
#define	BW_RATE		0x2C
#define	POWER_CTL	0x2D
#define	INT_ENABLE	0x2E
#define	INT_MAP		0x2F
#define	INT_SOURCE	0x30

#define	DATA_FORMAT	0x31
#define	DATAX0		0x32
#define	DATAX1		0x33
#define	DATAY0		0x34
#define	DATAY1		0x35
#define	DATAZ0		0x36
#define	DATAZ1		0x37

#define	FIFO_CTL	0x38
#define	FIFO_STATUS	0x39

/* register values for DEVID */
/* The device ID should always read this value, The customer does not
need to use this value but it can be read to check that the
/device can communicate */
#define ID				0xe5

/* Reserved soft reset value */
#define SOFT_RESET		0x52

/* Registers THRESH_TAP through TIME_INACT take only 8-bit values
There are no specific bit fields in these registers */
/* Bit values in ACT_INACT_CTL */
#define INACT_Z_ENABLE	0x01
#define INACT_Z_DISABLE	0x00
#define INACT_Y_ENABLE	0x02
#define INACT_Y_DISABLE	0x00
#define INACT_X_ENABLE	0x04
#define INACT_X_DISABLE	0x00
#define INACT_AC			0x08
#define INACT_DC			0x00
#define ACT_Z_ENABLE		0x10
#define ACT_Z_DISABLE		0x00
#define ACT_Y_ENABLE		0x20
#define ACT_Y_DISABLE		0x00
#define ACT_X_ENABLE		0x40
#define ACT_X_DISABLE		0x00
#define ACT_AC			0x80
#define ACT_DC			0x00

/* Registers THRESH_FF and TIME_FF take only 8-bit values
There are no specific bit fields in these registers */
/* Bit values in TAP_AXES */
#define TAP_Z_ENABLE		0x01
#define TAP_Z_DISABLE		0x00
#define TAP_Y_ENABLE		0x02
#define TAP_Y_DISABLE		0x00
#define TAP_X_ENABLE		0x04
#define TAP_X_DISABLE		0x00
#define TAP_SUPPRESS		0x08

/* Bit values in ACT_TAP_STATUS */
#define TAP_Z_SOURCE		0x01
#define TAP_Y_SOURCE		0x02
#define TAP_X_SOURCE		0x04
#define STAT_ASLEEP		0x08
#define ACT_Z_SOURCE		0x10
#define ACT_Y_SOURCE		0x20
#define ACT_X_SOURCE		0x40

/* Bit values in BW_RATE */
/* Expresed as output data rate */
#define RATE_3200			0x0f
#define RATE_1600			0x0e
#define RATE_800			0x0d
#define RATE_400			0x0c
#define RATE_200			0x0b
#define RATE_100			0x0a
#define RATE_50			0x09
#define RATE_25			0x08
#define RATE_12_5			0x07
#define RATE_6_25			0x06
#define RATE_3_125		0x05
#define RATE_1_563		0x04
#define RATE__782			0x03
#define RATE__39			0x02
#define RATE__195			0x01
#define RATE__098			0x00

/* Expressed as output bandwidth */
/* Use either the bandwidth or rate code,
whichever is more appropriate for your application */
#define BW_1600			0x0f
#define BW_800			0x0e
#define BW_400			0x0d
#define BW_200			0x0c
#define BW_100			0x0b
#define BW_50				0x0a
#define BW_25				0x09
#define BW_12_5			0x08
#define BW_6_25			0x07
#define BW_3_125			0x06
#define BW_1_563			0x05
#define BW__782			0x04
#define BW__39			0x03
#define BW__195			0x02
#define BW__098			0x01
#define BW__048			0x00
#define LOW_POWER			0x08
#define LOW_NOISE			0x00

/* Bit values in POWER_CTL */
#define WAKEUP_8HZ		0x00
#define WAKEUP_4HZ		0x01
#define WAKEUP_2HZ		0x02
#define WAKEUP_1HZ		0x03
#define SLEEP				0x04
#define MEASURE			0x08
#define STANDBY			0x00
#define AUTO_SLEEP		0x10
#define ACT_INACT_SERIAL	0x20
#define ACT_INACT_CONCURRENT	0x00

/* Bit values in INT_ENABLE, INT_MAP, and INT_SOURCE are identical.
Use these bit values to read or write any of these registers. */
#define OVERRUN			0x01
#define WATERMARK			0x02
#define FREEFALL			0x04
#define INACTIVITY		0x08
#define ACTIVITY			0x10
#define DOUBLETAP			0x20
#define SINGLETAP			0x40
#define DATAREADY			0x80

// Bit values in DATA_FORMAT
/* Register values read in DATAX0 through DATAZ1 are dependent on the
value specified in data format. Customer code will need to interpret
the data as desired. */
#define RANGE_2G			0x00
#define RANGE_4G			0x01
#define RANGE_8G			0x02
#define RANGE_16G			0x03
#define DATA_JUST_RIGHT	0x00
#define DATA_JUST_LEFT	0x04
#define 10BIT				0x00
#define FULL_RESOLUTION	0x08
#define INT_LOW			0x20
#define INT_HIGH			0x00
#define SPI3WIRE			0x40
#define SPI4WIRE			0x00
#define SELFTEST			0x80

// Bit values in FIFO_CTL
/* The low bits are a value 0 to 31 used for the watermark or the number
of pre-trigger samples when in triggered mode */
#define TRIGGER_INT1		0x00
#define TRIGGER_INT2		0x20
#define FIFO_MODE_BYPASS	0x00
#define FIFO_RESET		0x00
#define FIFO_MODE_FIFO	0x40
#define FIFO_MODE_STREAM	0x80
#define FIFO_MODE_TRIGGER	0xc0

// Bit values in FIFO_STATUS
/* The low bits are a value 0 to 32 showing the number of entries
currently available in the FIFO buffer */
#define FIFO_TRIGGERED	0x80
